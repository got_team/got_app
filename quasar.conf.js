/* eslint-disable no-unused-vars */
// Configuration for your app

module.exports = ctx => ({
  // app boot file (/src/boot)
  // --> boot files are part of "main.js"
  boot: [
    'i18n',
    'axios',
    'firebase',
    'logger',
  ],

  css: [
    'app.styl',
  ],

  extras: [
    'roboto-font',
    'material-icons', // optional, you are not bound to it
    // 'ionicons-v4',
    // 'mdi-v3',
    'fontawesome-v5',
    // 'eva-icons'
  ],

  framework: {
    // all: true, // --- includes everything; for dev only!

    components: [
      'QLayout',
      'QHeader',
      'QFooter',
      'QDrawer',
      'QPageContainer',
      'QPage',
      'QToolbar',
      'QToolbarTitle',
      'QBtn',
      'QCard',
      'QIcon',
      'QList',
      'QItem',
      'QItemSection',
      'QItemLabel',
      'QSelect',
      'QCardSection',
      'QCardActions',
      'QImg',
      'QSpinnerGears',
      'QMenu',
      'QInnerLoading',
    ],

    directives: [
      'Ripple',
      'ClosePopup',
    ],

    // Quasar plugins
    plugins: [
      'Notify',
      'Loading',
    ],

    // iconSet: 'ionicons-v4'
    // lang: 'de' // Quasar language
  },

  supportIE: false,

  build: {
    scopeHoisting: true,
    vueRouterMode: 'history',
    // vueCompiler: true,
    // gzip: true,
    // analyze: true,
    // extractCSS: false,
    extendWebpack(cfg) {
      cfg.module.rules.push({
        enforce: 'pre',
        test: /\.(js|vue)$/,
        loader: 'eslint-loader',
        exclude: /node_modules/,
      });

      cfg.module.rules.push({
        test: /\.pug$/,
        loader: 'pug-plain-loader',
      });
    },
  },

  devServer: {
    // https: true,
    // port: 8080,
    open: true, // opens browser window automatically
  },

  // animations: 'all' --- includes all animations
  animations: [
    'fadeIn',
    'fadeOut',
  ],

  ssr: {
    pwa: false,
  },

  pwa: {
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      importWorkboxFrom: 'cdn',
      exclude: [
        /\/icons\//,
        'manifest.json'],
    },
    manifest: {
      name: 'GoT Challenge',
      short_name: 'GoT Challenge',
      description: 'Challenge your friends and see who know more about Game of Thrones, because we know that Jon Snow knows nothing',
      display: 'standalone',
      orientation: 'portrait',
      background_color: '#39383D',
      theme_color: '#39383D',
      icons: [
        {
          src: 'statics/icons/android-icon-144x144.png',
          sizes: '128x128',
          type: 'image/png',
        },
        {
          src: 'statics/icons/android-icon-192x192.png',
          sizes: '192x192',
          type: 'image/png',
        },
        {
          src: 'statics/icons/icon-256x256.png',
          sizes: '256x256',
          type: 'image/png',
        },
        {
          src: 'statics/icons/android-chrome-384x384.png',
          sizes: '384x384',
          type: 'image/png',
        },
        {
          src: 'statics/icons/icon-512x512.png',
          sizes: '512x512',
          type: 'image/png',
        },
      ],
    },
  },
});

import Vue from 'vue';
import VueRouter from 'vue-router';
import { Loading, QSpinnerGears, Notify } from 'quasar';

import store from '../store';
import routes from './routes';

Vue.use(VueRouter);

Loading.setDefaults({
  spinner: QSpinnerGears,
  message: 'Taaam tam, tam tam taaaam tam, tam tam tam',
  messageColor: 'deep-orange-13',
  spinnerSize: 150,
  spinnerColor: 'deep-orange-13',
  customClass: 'bg-black',
});

const showLoading = () => {
  if (!Loading.isActive) {
    Loading.show();
  }
};

const getCharacters = async () => {
  if (!store.getters['character/characters'].length) {
    showLoading();
    await store.dispatch('character/getCharacters');
  }
};

const getUserData = async () => {
  if (!store.getters['bet/bet'].userId) {
    showLoading();
    await store.dispatch('bet/getUserBet');
  }

  if (store.getters['bet/bet'].userId && !store.getters['bet/betCharacters'].length) {
    showLoading();
    await store.dispatch('bet/getBetCharacters');
  }
};

const toHome = records => records.some(record => record.name === 'home');

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE,
  });

  Router.beforeEach((to, from, next) => {
    Vue.prototype.$firebase.auth().onAuthStateChanged(async (currentUser) => {
      try {
        await getCharacters();

        if (currentUser) {
          await getUserData();
        }
      } catch (err) {
        Vue.prototype.$log.error(err);
        Loading.hide();
        Notify('Something happens, try again');
      }

      if (currentUser && toHome(to.matched)) {
        next({ name: 'app' });
      }

      if (to.matched.some(record => record.meta.auth)) {
        if (!currentUser) {
          next({
            name: 'login',
            query: { redirect: to.fullPath },
          });
        }
      }
      next(); // make sure to always call next()!
    });
  });


  return Router;
}

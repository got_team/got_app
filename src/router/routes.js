/* eslint-disable global-require */

const routes = [
  {
    path: '/',
    component: () => import('layouts/Public.vue'),
    children: [
      {
        path: '',
        name: 'home',
        component: () => import('pages/Home.vue'),
        meta: {
          page: 1,
        },
      },
      {
        path: 'login',
        name: 'login',
        component: () => import('pages/Login.vue'),
        meta: {
          page: 1,
        },
      },
      {
        path: 'zrp',
        name: 'zrp',
        component: () => import('pages/ZrpRanking.vue'),
        meta: {
          page: 1,
        },
      },
    ],
  },
  {
    path: '/app',
    component: () => import('layouts/Logged.vue'),
    meta: {
      auth: true,
    },
    children: [
      {
        path: '',
        name: 'app',
        component: () => import('pages/Index.vue'),
        meta: {
          page: 2,
        },
      },
      {
        path: 'profile',
        name: 'profile',
        component: () => import('pages/Profile.vue'),
        meta: {
          page: 1,
        },
      },
      {
        path: 'zrp',
        redirect: {
          name: 'zrp',
        },
      },
    ],
  },
];

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue'),
  });
}

export default routes;

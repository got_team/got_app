/*
export function someAction (context) {
}
*/

let lastVisibleDocument = {};

import Vue from 'vue';
import functionsService from '../../services/functions';

export const getBets = async ({ commit }) => {
  const snapshot = await Vue.prototype.$firestore.collection('bets')
    .orderBy('points', 'desc').orderBy('updateAt')
    .startAfter(lastVisibleDocument)
    .limit(10)
    .get();

  lastVisibleDocument = snapshot.docs[snapshot.docs.length - 1];

  const data = snapshot.docs.map(doc => doc.data());
  commit('setBets', data);
};

export const getZrpBets = async ({ commit }) => {
  const data = await functionsService.getZrpBets();

  commit('setZrpBets', data);
};

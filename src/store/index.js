import Vue from 'vue';
import Vuex from 'vuex';

import bet from './bet';
import character from './character';
import stats from './stats';
import ranking from './ranking';

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

const Store = new Vuex.Store({
  modules: {
    bet,
    character,
    stats,
    ranking,
  },

  // enable strict mode (adds overhead!)
  // for dev mode only
  strict: process.env.DEV,
});

export default Store;

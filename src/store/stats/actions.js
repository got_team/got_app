/*
export function someAction (context) {
}
*/

import Vue from 'vue';

export const getStats = async ({ commit }) => {
  const snapshot = await Vue.prototype.$firestore.collection('stats').orderBy('createdAt', 'desc').limit(1).get();
  const data = snapshot.docs[0].data();
  commit('setStats', data);
};

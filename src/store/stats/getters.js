
export const stats = state => state.stats;
export const mostKilledCharacters = state => state.stats.mostKilledCharacters.slice(0, 6);
export const mostSavedCharacters = state => state.stats.mostSavedCharacters.slice(0, 6);
export const whoWillBeNissaNissa = state => state.stats.whoWillBeNissaNissa.slice(0, 6);
export const whoWillBeAzorAhai = state => state.stats.whoWillBeAzorAhai.slice(0, 6);
export const whoWillBeKing = state => state.stats.whoWillBeKing.slice(0, 6);
export const whoWillKillCersei = state => state.stats.whoWillKillCersei.slice(0, 6);

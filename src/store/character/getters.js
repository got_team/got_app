/*
export function someGetter (state) {
}
*/

export const characters = state => state.characters;

export const charactersAlive = state => state.characters.filter(character => character.isAlive);

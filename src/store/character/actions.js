/*
export function someAction (context) {
}
*/

import Vue from 'vue';

// console.log(Vue.prototype.$t('none_selection'))

export const getCharacters = async ({ commit }) => {
  const snapshot = await Vue.prototype.$firestore.collection('characters').get();
  const data = snapshot.docs.map(doc => doc.data());
  commit('setCharacters', data);
};

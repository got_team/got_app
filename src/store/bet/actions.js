/*
export function someAction (context) {
}
*/

import Vue from 'vue';
import functionsService from '../../services/functions';

const userId = () => Vue.prototype.$firebase.auth().currentUser.uid;

export const getUserBet = async ({ commit }) => {
  const snapshot = await Vue.prototype.$firestore.collection('bets').where('userId', '==', userId()).get();
  let data = {};
  if (snapshot.docs.length) {
    data = snapshot.docs[0].data();
  }
  commit('setUserBet', data);
};

export const getBetCharacters = async ({ commit }) => {
  const snapshot = await Vue.prototype.$firestore.collection('bets').doc(userId()).collection('characters').get();
  const characters = snapshot.docs.map(doc => doc.data());
  commit('setBetCharacters', characters);
};

export const createCharacterBet = async ({ commit }, payload) => {
  await functionsService.createCharacterBet(payload);
  commit('addCharacter', payload);
};

export const createAnswerBet = async ({ commit }, payload) => {
  await functionsService.createAnswerBet(payload);
  commit('addAnswer', payload);
};

/*
export function someMutation (state) {
}
*/

export const setUserBet = (state, data) => {
  state.bet = { ...state.bet, ...data };
};

export const setBetCharacters = (state, data) => {
  state.bet.characters = data;
};

export const addCharacter = (state, data) => {
  state.bet.characters.push(data);
};

export const addAnswer = (state, data) => {
  state.bet = { ...state.bet, ...data };
};


import Vue from 'vue';

const createCharacterBet = async (payload) => {
  const token = await Vue.prototype.$firebase.auth().currentUser.getIdToken();
  const { data } = await Vue.prototype.$axios({
    method: 'post',
    url: 'https://us-central1-got-challenge.cloudfunctions.net/app/characters',
    data: payload,
    headers: { Authorization: `Bearer ${token}` },
  });
  return data;
};

const createAnswerBet = async (payload) => {
  const token = await Vue.prototype.$firebase.auth().currentUser.getIdToken();
  const { data } = await Vue.prototype.$axios({
    method: 'post',
    url: 'https://us-central1-got-challenge.cloudfunctions.net/app/answers',
    data: payload,
    headers: { Authorization: `Bearer ${token}` },
  });
  return data;
};

const warmUpFunctions = async () => {
  try {
    Vue.prototype.$axios.get('https://us-central1-got-challenge.cloudfunctions.net/app/status');
  } catch (err) {
    Vue.prototype.$log.error(err);
  }
};

const getZrpBets = async () => {
  const { data } = await Vue.prototype.$axios.get('https://us-central1-got-challenge.cloudfunctions.net/app/zrp');
  return data;
};

export default {
  createCharacterBet,
  warmUpFunctions,
  createAnswerBet,
  getZrpBets,
};

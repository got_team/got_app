import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/performance';
import * as firebaseui from 'firebaseui';

const config = {
  apiKey: 'AIzaSyAYFlIla5KFAZ6OQpAbBxQMy3tAvUtjiZk',
  authDomain: 'got-challenge.firebaseapp.com',
  databaseURL: 'https://got-challenge.firebaseio.com',
  projectId: 'got-challenge',
  storageBucket: 'got-challenge.appspot.com',
  messagingSenderId: '668105756606',
  appId: '1:668105756606:web:ad31363a93d7d886',
};

firebase.initializeApp(config);

firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL);

firebase.firestore().enablePersistence();

const DB = firebase.firestore();

const PERF = firebase.performance();

const AUTH_UI = new firebaseui.auth.AuthUI(firebase.auth());

export default ({ Vue }) => {
  Vue.prototype.$authUI = AUTH_UI;
  Vue.prototype.$firebase = firebase;
  Vue.prototype.$firestore = DB;
  Vue.prototype.$perf = PERF;
};
